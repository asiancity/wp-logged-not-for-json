<?php
/*
Plugin Name: Logged-in-only not for Json
Plugin URI: https://github.com/reimersjan/wp-logged-in-only
Description: logged-in not for REST API.
Author: Anh-Tuan Hoang
Version: 1.0.0
Author URI: mainstream-berlin.de
License: GPLv2
*/
/*
  Change plugin logged_in_only
*/

if ( function_exists( "logged_in_only_rest_api" ) ){
  function no_logged_in_only_rest_api( $result ) {

    if ( !is_user_logged_in() && array_key_exists("rest_not_logged_in", $result->errors ) ) {
       return false;
    }

    if ( ! empty( $result ) ) {
    		return $result;
    }

    return $result;
  }
  add_filter( 'rest_authentication_errors', 'no_logged_in_only_rest_api', 99, 3 );
}
?>
